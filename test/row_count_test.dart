import 'package:calendar_fast/utils/row_calendar_count.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Test Row count', () {
    test('Test Jan 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 1)), 6);
    });
    test('Test Feb 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 2)), 5);
    });
    test('Test March 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 3)), 5);
    });
    test('Test Apr 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 4)), 5);
    });
    test('Test May 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 5)), 5);
    });
    test('Test June 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 6)), 5);
    });

    test('Test July 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 7)), 6);
    });

    test('Test Aug 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 8)), 5);
    });
    test('Test Sep 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 9)), 5);
    });

    test('Test October 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 10)), 6);
    });
    test('Test November 2022', () async {
      expect(rowCalendarCount(DateTime(2022, 11)), 5);
    });
  });
}
