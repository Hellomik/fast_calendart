int getPreDayCount(DateTime dateTime) {
  DateTime firstDay = DateTime(dateTime.year, dateTime.month);

  return firstDay.weekday % 7;
}

int rowCalendarCount(DateTime dateTime) {
  DateTime firstDay = DateTime(dateTime.year, dateTime.month);
  DateTime lastDay = DateTime(dateTime.year, dateTime.month + 1);

  int dayCount = lastDay.difference(firstDay).inDays;
  int preDayCout = getPreDayCount(dateTime);
  final generalDay = preDayCout + dayCount;
  return (generalDay / 7).ceil();
}
