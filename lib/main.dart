import 'package:calendar_fast/molecules/calendar_card.dart';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        textTheme: const TextTheme(
          subtitle1: TextStyle(
            color: Color(0xFFBDBDBD),
          ),
        ),
      ),
      home: Scaffold(
        body: Center(
          child: CalendarCard(
            dateTime: DateTime(2022, 10, 1),
            superDays: [
              DateTime(2022, 10, 8),
              DateTime(2022, 10, 19),
            ],
          ),
        ),
      ),
    );
  }
}
