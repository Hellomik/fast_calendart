import 'package:calendar_fast/utils/colors.dart';
import 'package:flutter/material.dart';

class CalendarChip extends StatelessWidget {
  final String title;
  final bool isOut;
  final bool active;
  final bool isRounded;

  const CalendarChip({
    required this.title,
    this.isOut = false,
    this.active = false,
    this.isRounded = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final BoxDecoration boxDecoration;
    final TextStyle textStyle;
    if (isOut) {
      boxDecoration = const BoxDecoration();
      textStyle = TextStyle(
        fontWeight: FontWeight.w700,
        color: Theme.of(context).textTheme.subtitle1?.color,
      );
    } else if (active) {
      boxDecoration = BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(4),
      );
      textStyle =
          const TextStyle(fontWeight: FontWeight.w700, color: Colors.white);
    } else if (isRounded) {
      boxDecoration = BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          width: 2,
          color: Theme.of(context).primaryColor,
        ),
      );
      textStyle = const TextStyle(fontWeight: FontWeight.w700);
    } else {
      boxDecoration = const BoxDecoration();
      textStyle = const TextStyle(fontWeight: FontWeight.w700);
    }
    return Container(
      decoration: boxDecoration,
      alignment: Alignment.center,
      height: 30,
      width: 30,
      child: Text(
        title,
        style: textStyle,
      ),
    );
  }
}
