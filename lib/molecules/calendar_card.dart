import 'package:calendar_fast/molecules/calendar_chip.dart';
import 'package:calendar_fast/utils/colors.dart';
import 'package:calendar_fast/utils/row_calendar_count.dart';
import 'package:flutter/material.dart';
import 'package:calendar_fast/utils/compare_date_time.dart';

final daysWeek = [
  'S',
  'M',
  'T',
  'W',
  'T',
  'F',
  'S',
];

final monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

class CalendarCard extends StatefulWidget {
  final DateTime dateTime;
  final List<DateTime> superDays;

  const CalendarCard({
    required this.dateTime,
    this.superDays = const [],
    super.key,
  });

  @override
  State<CalendarCard> createState() => _CalendarCardState();
}

class _CalendarCardState extends State<CalendarCard> {
  DateTime get monthDateStart =>
      DateTime(widget.dateTime.year, widget.dateTime.month);
  DateTime get monthDateEnd =>
      DateTime(widget.dateTime.year, widget.dateTime.month + 1).subtract(
        const Duration(days: 1),
      );

  DateTime? choosenDateTime;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            monthNames[widget.dateTime.month - 1],
            style: const TextStyle(fontWeight: FontWeight.w700),
          ),
          Text(
            widget.dateTime.year.toString(),
            style: const TextStyle(fontWeight: FontWeight.w700),
          ),
          const SizedBox(height: 16),
          Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.circular(6),
                ),
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  children: List.generate(
                    daysWeek.length,
                    (rowIndex) => Expanded(
                      child: Center(
                        child: Text(
                          daysWeek[rowIndex],
                          style: const TextStyle(fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              ...List.generate(
                rowCalendarCount(widget.dateTime) * 2 - 1,
                (columnInd) => columnInd.isOdd
                    ? const SizedBox(height: 10)
                    : Row(
                        children: List.generate(
                          7,
                          (rowIndex) {
                            final columnIndex = columnInd ~/ 2;
                            final DateTime curTime = monthDateStart.add(
                                Duration(
                                    days: columnIndex * 7 +
                                        rowIndex -
                                        getPreDayCount(monthDateStart)));
                            final isOut = curTime < monthDateStart ||
                                monthDateEnd < curTime;
                            return Expanded(
                              child: GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: isOut
                                    ? null
                                    : () {
                                        setState(() {
                                          choosenDateTime = curTime;
                                        });
                                      },
                                child: Center(
                                  child: CalendarChip(
                                    isOut: isOut,
                                    active: curTime == choosenDateTime,
                                    isRounded:
                                        widget.superDays.contains(curTime),
                                    title: curTime.day.toString(),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
